#include <stdio.h>

struct node {
    struct node *left;
    struct node *right;
char *name;
};
struct node *creator(char *name) {
   struct node * r= (struct node *)malloc(sizeof(struct node));
    r->name = name;
    return r;
}
void view(struct node* root) {
    static int a = 0;
    int i = a;
    static int prLev = 0, maxLev = 0;
    if (root) {
        while (i--) {
           printf("\t");
            if(i == 1 && prLev!=maxLev) {
                printf("|");
            }
        }
    i = a;
        if(root->left && root->right)
        {
            ++prLev;
            if(prLev > maxLev)
            {
                maxLev = prLev;
            }
        }
        if(a !=0) {
                if(prLev==maxLev)
                    printf("|\n");
                while(i--) {
                    printf("\t");
                    if(prLev!=maxLev)
                        printf("|");
                }
        }
                printf("%s\n", root->name);
        ++a;
        view(root->left);
        view(root->right);
        --a;

    }
}
int main(void) {
    struct node *stack;

   stack = creator("<Имя потомка>");
   stack->left = creator("-<имя мамы>");
   stack->right = creator("-<Имя папы>");
   stack->left->left = creator("-<Имя бабушки  1>");
   stack->left->right = creator("-<Имя деда 1>");
   stack->right->left = creator("-<Имя бабушки 2>");
   stack->right->right = creator("-<Имя деда 2>");

    view(stack);
    return 0;
}